# Jupyter

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fjailbreak%2Fjupyter-lab-experiments/master)

Testing jupyterlab technology

## Install

```bash
pip install -r requirements.txt
jupyter labextension install @jupyter-widgets/jupyterlab-manager --no-build
jupyter labextension install bqplot --no-build
jupyter labextension install jupyter-leaflet --no-build
jupyter labextension install @jupyter-voila/jupyterlab-preview --no-build
jupyter lab build
```

## Run

```bash
jupyter lab
```

or

```bash
voila {mynotebook}.ipynb
```

## Notebooks

- `bqplot_barchart_update.ipnb`: bqplot bar chart sample that updates data on the fly
- `bqplot_map.ipynb`: bqplot France map sample
- `bqplot_piechart.ipynb`: bqplot pie chart that works around labels display on init
- `bqplot_piechart_tooltip.ipynb`: bqplot pie chart with tooltip
- `dropdown.ipynb`: minimal ipywidgets drop-down sample
- `ipyleaflet_geojson.ipynb`: display ODSERVATOIRE geojson on a map
- `issue_piechart_change.ipynb`: show bug on bqplot pie chart display (see [issue 991](https://gi`thub.com/bloomberg/bqplot/issues/991))
- `issue_piechart_labels_layout.ipynb`: enlighten layout problems when pie chart update with labels
- `issue_piechart_labels.ipynb`: another version of bqplot_piechart.ipynb (see [issue 989](https://gi`thub.com/bloomberg/bqplot/issues/989))
- `load_data.ipynb`: common code to load data
- `platforms.ipynb`: some piecharts about platforms statistics
- `OpenDataDashboard.ipynb`: first version of OpenData dashboard
- `WithDeptOpenDataDashboard.ipynb`: latest version with region / department filter
